<style>
    #map{
        width:100%;
        height:510px;
    }
    body{
        margin: 0;
    }
</style>
<link href="{{ URL::asset('bower_components/leaflet/dist/leaflet.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ URL::asset('bower_components/leaflet-draw/dist/leaflet.draw.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ URL::asset('bower_components/leaflet-geosearch/src/css/l.geosearch.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ URL::asset('bower_components/leaflet-routing-machine/dist/leaflet-routing-machine.css') }}" rel="stylesheet" type="text/css"/>
@include('layouts/icons')
<div id="map-block"><div id="map"></div></div>

<div id="scripts">
    <div id="leaflet">
        <script src="{{ URL::asset('bower_components/jquery/dist/jquery.min.js') }}"></script>
        <script src="{{ URL::asset('bower_components/leaflet/dist/leaflet.js') }}"></script>
        <script src="{{ URL::asset('bower_components/leaflet-draw/dist/leaflet.draw.js') }}"></script>
        <script src="{{ URL::asset('bower_components/leaflet-geosearch/src/js/l.control.geosearch.js') }}"></script>
        <script src="{{ URL::asset('bower_components/leaflet-geosearch/src/js/l.geosearch.provider.openstreetmap.js') }}"></script>
        <script src="{{ URL::asset('bower_components/leaflet-routing-machine/dist/leaflet-routing-machine.js') }}"></script>
        <script src="{{ URL::asset('bower_components/LeafletPlayback/dist/LeafletPlayback.js') }}"></script>
    </div>
    <div id="custom">
        @yield('js')
    </div>
</div>