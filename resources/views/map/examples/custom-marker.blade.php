@extends('map.map')
@section('js')
<script id="script">
/**
* For this you need install and include in your project
* leaflet.js
* leaflet-draw.js
*/

var map = L.map("map").setView([55.675, 37.542], 11);
var geozones = new L.FeatureGroup();
L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'}).addTo(map);
map.addControl(new L.Control.Draw({draw:{polyline:false, rectangle:false, circle:false, polygon:false},
    edit:{featureGroup: geozones}}));


function getIcon(lat){
    var icon = lat < 55.64 ? 'red' : lat > 55.7 ? 'yellow' : 'blue';
    return L.icon({ iconUrl: '../../../img/examples/' + icon + '-home.png', iconSize:[40, 40] });
}

map.on('draw:created', function(e){
    var marker = L.marker(e.layer._latlng, { icon: getIcon(e.layer._latlng.lat) });
    geozones.addLayer(marker);
    map.addLayer(geozones);
});

map.on('draw:deleted', function(e){
    geozones.removeLayer(e.layer);
});
</script>
@endsection
                
