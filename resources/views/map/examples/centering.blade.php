@extends('map.map')
@section('js')
<script id="script">

/**
* For this you need install and include in your project
* leaflet.js
* leaflet-draw.js
*/

var map = L.map("map").setView([55.675, 37.542], 11);
var geozones = new L.FeatureGroup();
L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'}).addTo(map);
map.addControl(new L.Control.Draw({edit:{featureGroup: geozones}}));

/**
* Get Center point of polygone
* @param {Object} layer
* @returns {L.LatLng}
*/
function getCenterPolygon(layer){
    if(layer._latlng){
return layer._latlng;
    }else{
var bound = layer.getBounds();
return new L.LatLng(bound._northEast.lat - (bound._northEast.lat - bound._southWest.lat)/2, bound._northEast.lng - (bound._northEast.lng - bound._southWest.lng)/2);
    }
}

map.on('draw:created', function(e){
    var center = getCenterPolygon(e.layer);
    geozones.addLayer(e.layer.bindPopup("Center of layer:<br/>lat: " + center.lat + "<br/>lng: " +center.lng));
    map.addLayer(geozones);
    map.panTo(center);
});

map.on('draw:deleted', function(e){
    geozones.removeLayer(e.layer);
});
</script>
@endsection

