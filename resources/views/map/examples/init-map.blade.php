@extends('map.map')
@section('js')
<script id="script">
/**
 * For this you need install and include in your project
 * leaflet.js
 */

var map = L.map("map").setView([55.675, 37.542], 6);

mapLayers = {
    "Yandex": new L.tileLayer("http://vec01.maps.yandex.net/tiles?v=1.26.0&l=map&x={x}&y={y}&z={z}", {maxZoom: 20}),
    "OSM": new L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>'}),
    "Google": new L.tileLayer('http://{s}.google.com/vt/lyrs=p&x={x}&y={y}&z={z}',{maxZoom: 20, subdomains:['mt0','mt1','mt2','mt3']}),
    "Yandex Satellite": new L.tileLayer("http://sat00.maps.yandex.net/tiles?l=sat&x={x}&y={y}&z={z}", {maxZoom: 20}),
    "Google Satellite": new L.tileLayer('http://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}',{maxZoom: 20, subdomains:['mt0','mt1','mt2','mt3']}),
};

mapLayers.OSM.addTo(map);
map.addControl(new L.Control.Layers(mapLayers, null, { position: 'bottomleft' }));
</script>
@endsection
                
