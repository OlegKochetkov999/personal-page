@extends('map.map')
@section('js')
<script id="script">
/**
* For this you need install and include in your project
* leaflet.js
* leaflet-draw.js
* leaflet-geosearch.js
*/

var map = L.map("map").setView([55.675, 37.542], 11);
var geozones = new L.FeatureGroup();
L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'}).addTo(map);
map.addControl(new L.Control.Draw({edit:{featureGroup: geozones}}));
map.addControl(new L.Control.GeoSearch({provider: new L.GeoSearch.Provider.OpenStreetMap, showMarker: false}));

/**
* Get Center point of polygone
* @param {Object} layer
* @returns {L.LatLng}
*/
function getCenterPolygon(layer){
    if(layer._latlng){
        return layer._latlng;
    }else{
        var bound = layer.getBounds();
        return new L.LatLng(bound._northEast.lat - (bound._northEast.lat - bound._southWest.lat)/2, bound._northEast.lng - (bound._northEast.lng - bound._southWest.lng)/2);
    }
};

/**
 * This method uses nominatim service to get address by latlng coordinates
 * @param {L.LatLng} latlng
 * @returns {String}
 */
function getAddressByCoordinates(latlng){
    var place = "Address not found";
    $.ajax({
        type: 'GET',
        headers: {"Accept-Language": "ru-ru,ru;q=0.8,en-us;q=0.6,en;q=0.4" },
        dataType: 'json',
        async: false,
        url: "http://nominatim.openstreetmap.org/reverse?format=json&lat=" + latlng.lat + "&lon=" + latlng.lng + "&zoom=18&addressdetails=1",
        success: function (data) {
            if(typeof data != "undefined" && typeof data.address != "undefined"){
                var address = data.address;
                var country =  typeof address.country != "undefined" ? address.country : "";
                var state =  typeof address.state != "undefined" ? ", " + address.state : "";
                var road =  typeof address.road != "undefined" ? ", " + address.road : "";
                var house = typeof address.house_number != "undefined" ? ", " + address.house_number : "";
                place = country + state + road + house;
            }
        },
    });
    return place;
};

map.on('draw:created', function(e){
    var place = getAddressByCoordinates(getCenterPolygon(e.layer));
    geozones.addLayer(e.layer.bindPopup("Your address: " + place));
    map.addLayer(geozones);
});

map.on('draw:deleted', function(e){
    geozones.removeLayer(e.layer);
});
</script>
@endsection
                
