@extends('map.map')
@section('js')
<script id="script">
/**
* For this you need install and include in your project
* leaflet.js
* leaflet-routing-machine.js
*/

var map = L.map("map").setView([55.675, 37.542], 11);
L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'}).addTo(map);
map.addControl(new L.Control.GeoSearch({provider: new L.GeoSearch.Provider.OpenStreetMap, showMarker: false}));

routingControl = L.Routing.control({
    lineOptions: { styles: [ { color: 'red', opacity: 1, weight: 6 }] },
    waypoints: [new L.LatLng(55.675, 37.542), new L.LatLng(55.775, 37.672)],
});

map.addControl(routingControl);
routingControl.hide();
</script>
@endsection
                
