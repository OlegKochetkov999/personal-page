@extends('map.map')
@section('js')
<script id="script">

/**
* For this you need install and include in your project
* leaflet.js
*/

var map = L.map("map").setView([55.675, 37.542], 11);
L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'}).addTo(map);

var layers = [];
var coordinates = [
    new L.LatLng(55.675, 37.542),
    new L.LatLng(55.685, 37.532),
    new L.LatLng(55.665, 37.552),
    new L.LatLng(55.655, 37.522),
    new L.LatLng(55.633, 37.532)
];

coordinates.forEach(function(value){
    var layer = new L.Marker(value);
    map.addLayer(layer);
    layers.push(layer);
});

L.Control.Centering = L.Control.extend({
    options: {
        position: 'topleft',
        title: 'Centering on one of the layers'
    },
    initialize: function (options) {
        L.Util.setOptions(this, options);
    },
    onAdd: function (map) {
        var container = L.DomUtil.create('div', 'leaflet-bar leaflet-control leaflet-control-custom');
        const icon =
                `<svg viewBox="-3 -3 35 35">
                    <use xlink:href="#search"></use>
                </svg>`;
        $(container).append(icon);
        container.title = this.options.title;
    
        container.style.cursor = 'pointer';
        container.style.backgroundColor = 'white';
        container.style.textAlign = 'center';
        container.style.lineHeight = '2';
        container.style.width = '26px';
        container.style.height = '26px';
 
        container.onclick = function(){
            var number = 1 + Math.floor(Math.random() * 4);
            map.panTo(layers[number]._latlng);
        };
    
        return container;
    },
});

map.addControl(new L.Control.Centering());
</script>
@endsection

