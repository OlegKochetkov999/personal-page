@extends('map.map')
@section('js')
<script id="script">
/**
 * For this you need install and include in your project
 * leaflet.js
 * leaflet-draw.js
 */

var map = L.map("map").setView([55.675, 37.542], 11);
var geozones = new L.FeatureGroup();
L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'}).addTo(map);
map.addControl(new L.Control.Draw({edit:{featureGroup: geozones}}));

map.on('draw:created', function(e){
    geozones.addLayer(e.layer);
    map.addLayer(geozones);
});

map.on('draw:edited', function(e){
    /**
     * Some code
     */
});

map.on('draw:deleted', function(e){
    geozones.removeLayer(e.layer);
});
</script>
@endsection

