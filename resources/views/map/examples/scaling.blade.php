@extends('map.map')
@section('js')
<script id="script">

/**
* For this you need install and include in your project
* leaflet.js
* leaflet-draw.js
*/

var map = L.map("map").setView([55.675, 37.542], 10);
var geozones = new L.FeatureGroup();
L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'}).addTo(map);
map.addControl(new L.Control.Draw({draw: {marker: false}, edit:{featureGroup: geozones}}));

map.on('draw:created', function(e){
    geozones.addLayer(e.layer);
    map.addLayer(geozones);
    map.fitBounds(e.layer.getBounds());
});

map.on('draw:deleted', function(e){
    geozones.removeLayer(e.layer);
});
</script>    
@endsection

