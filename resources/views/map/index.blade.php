@extends('layouts.app-extend')
@section('css')
    <link href="{{ URL::asset('/css/map.css') }}" rel="stylesheet" type="text/css"/>
    <style>
        #editor {
        width: inherit;
        height: inherit;
    }
    </style>
@endsection

@section('content')
    <div class="container-extend">
        <div class="container-title">
            <div class="container-content">
                <div class="block-description">
                    <span class="description"><a href="/" class="nav">Главная</a> / Песочница</span>
                </div>
            </div>
        </div>
        <div class="container-main">
            <div class="main-navigation">
                <select id="menu-example" class="input-submit">
                    <?php foreach($types as $key => $value): ?>
                    <option value="<?= $key ?>"><?= $value; ?></option>
                    <?php endforeach; ?>
                </select>
                <input type="button" id="code-run" class="input-submit" value="Run" />
            </div>
            <div class="main-code">
                <div id="editor" class="code-area"></div>
            </div>
            <iframe id='block-iframe' src="/map/block/1" class="main-map" data-src="/map/block/1"></iframe>
        </div>
    </div>
@endsection
@section('js')
    <!--script src="{{ URL::asset('bower_components/leaflet/dist/leaflet.js') }}"></script>
    <script src="{{ URL::asset('bower_components/leaflet-draw/dist/leaflet.draw.js') }}"></script-->
    <script src="{{ URL::asset('bower_components/ace-builds/src/ace.js') }}" type="text/javascript" charset="utf-8"></script>
    <script src="{{ URL::asset('bower_components/ace-builds/src/mode-javascript.js') }}" type="text/javascript" charset="utf-8"></script>
    <script src="{{ URL::asset('js/code.js') }}"></script>
@endsection