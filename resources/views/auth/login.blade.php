@extends('layouts.app')

@section('content')
<div class="container" style="padding:20px">
    <p class="title">Sorry, but this page just for me :)</p>
    <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
        {!! csrf_field() !!}
        <span class="default">E-Mail Address</span>
        <input type="email" style="display: inline-block; width:170px;margin-left: 5px;" class="input-text" name="email" value="{{ old('email') }}">
        <br />
        <span class="default">Password</span>
        <input style="display: inline-block; width:170px;margin-left: 45px" type="password" class="input-text" name="password">
        <br />
        <input type="submit" class="input-submit" value="Login" />
    </form>
</div>
@endsection
