<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="yandex-verification" content="2d2c764fcf86ceaf" />
    <meta name='wmail-verification' content='aa28d3712bb3ca477e6dece2c7f42b5e' />
    <link href="{{ URL::asset('/css/layout.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::asset('/css/layout.extend.css') }}" rel="stylesheet" type="text/css"/>
    @yield('css')
    <title>homely web</title>
</head>
<body id="app-layout">
    @yield('menu')
    <div class="pre-content">
        <div class="content-extend">
            @yield('content')
        </div>
    </div>
    <script src="{{ URL::asset('bower_components/jquery/dist/jquery.min.js') }}"></script>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-90843109-1', 'auto');
        ga('send', 'pageview');

    </script>
    @yield('js')
</body>
</html>
