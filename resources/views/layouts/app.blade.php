<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="yandex-verification" content="2d2c764fcf86ceaf" />
    <meta name='wmail-verification' content='aa28d3712bb3ca477e6dece2c7f42b5e' />
    <link href="{{ URL::asset('/css/layout.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::asset('/css/animate.css') }}" rel="stylesheet" type="text/css"/>
    @yield('css')
    <title>homely web</title>
</head>
<body id="app-layout">
    @include('layouts/icons')
    <header class='header'>
        <a class="logo" href="{{ url('/') }}">{{ Html::image('/img/logo.png') }}</a>
    </header>
    @yield('menu')
    <div class="pre-content">
        <div class="content">
            @yield('content')
        </div>
        <div class='menu'>
            <div id="menu-search" class="menu-button">
                <div class="menu-button-inner">
                    <div class="menu-button-content search"></div>
                </div>
            </div>
            <div id="menu-category" class="menu-button">
                <div class="menu-button-inner">
                    <div class="menu-button-content category"></div>
                </div>
            </div>
            <div id="menu-map" class="menu-button">
                <a href="/map" title="map">
                    <div class="menu-button-inner">
                        <div class="menu-button-content map"></div>
                    </div>
                </a>    
            </div>
            <div id="menu-linkedin" class="menu-button">
                <a href="https://www.linkedin.com/in/%D0%BE%D0%BB%D0%B5%D0%B3-%D0%BA%D0%BE%D1%87%D0%B5%D1%82%D0%BA%D0%BE%D0%B2-468ba1103" target="blank">
                    <div class="menu-button-inner">
                        <div class="menu-button-content linkedin"></div>
                    </div>
                </a>
            </div>
            <div id="menu-email" class="menu-button">
                <div class="menu-button-inner">
                    <div class="menu-button-content email"></div>
                </div>
            </div>
        </div>
        <div id="email-modal" class="hide">
            <div class="container-title">
                <div class="container-content">
                    {{ Html::image('/img/email-icon.png', 'email icon', ['class' => 'email-icon']) }}
                    <div class="block-description">
                        <span class="description" style="padding-left: 5px">
                            Обратная связь
                        </span>
                    </div>
                </div>
            </div>
            <form id="email-form" action="{{ URL::to('email/send') }}" method="POST" >
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input name="email" type="email" placeholder="Адрес электронной почты" class="input-text" />
                <textarea name="content" class="textarea" style="height:180px" placeholder="Сообщение"></textarea>
                <input type="submit" value="Отправить" class="input-submit" />
            </form>
        </div>
        <div id="background-modal" class="hide"></div>
        <div id="notice" class="hide">
            <div class="container-content">
                <div class="block-icon"></div>
                <div class="block-description">
                    <span id="notice-message" class="description"></span>
                </div>
            </div>
        </div>
    </div>
    <footer class='footer'></footer>
    <script src="{{ URL::asset('bower_components/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ URL::asset('js/menu.js') }}"></script>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-90843109-1', 'auto');
        ga('send', 'pageview');

    </script>
    @yield('js')
</body>
</html>
