@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="edit-post">
            <div class="panel-heading"><p class="title">New Post</p></div>
            @include('common.errors')
            <form action="<?php if($post): ?> {{ url('post/edit/' . $post->id) }} <?php else: ?> {{ url('post/create') }} <?php endif; ?>" method="POST" class="form-horizontal">
                {{ csrf_field() }}
                <p class="default">Title:</p> 
                <input type="text" name="title" id="post-title" class="input-text" value="<?php if($post): echo $post->title; endif?>" />
                <p class="default">Heading:</p> 
                <select multiple="true" name="heading[]" id="post-heading" class="form-control" >
                    <?php $array = []; ?>
                    <?php if($post AND $post->heading): ?>
                        <?php $array = explode(',', $post->heading); ?>
                    <?php endif; ?>
                    @foreach($headings as $key => $value)
                        <option value="{{$value}}" <?php if(in_array($value, $array)): echo "selected"; endif;?> >{{$key}}</option>
                    @endforeach
                </select>
                <select name="type" id="post-type" class="form-control" >
                    @foreach($types as $key => $value)
                        <option value="{{$value}}" <?php if($post AND $post->type == $value): echo "selected"; endif;?> >{{$key}}</option>
                    @endforeach
                </select>
                <p class="default">Content:</p> 
                <textarea name="content" id="post-content" class="edit-textarea"><?php if($post): echo $post->content; endif?></textarea>
                <p class="default">Description:</p>
                <textarea name="description" id="post-description" class="textarea"><?php if($post): echo $post->description; endif?></textarea>
                <input type="submit" class="input-submit" value="Save Post" />
            </div>    
        </form>
    </div>
@endsection


