@extends('layouts.app')

@section('content')
    <div class="container">
        <form action="{{ url('/post/edit/0') }}" method="GET" class="">
            {{ csrf_field() }}
            <input type="submit" class="submit-new-post input-submit" value="New Post"/>
        </form>
        @if(count($posts) > 0)
            <div class="list">
                @foreach($posts as $post)
                    <div class="post">
                        <a class="post-title" href="/post/{{ $post->id }}"><?= $post->title; ?></a>
                        <form class="action" action="{{url('post/edit/' . $post->id)}}" method="GET">
                            {{ csrf_field() }}
                            <input class="input-submit" type="submit" id="edit-post-{{ $post->id }}" value="Edit" />
                        </form>
                        <form class="action" action="{{url('post/remove/' . $post->id)}}" method="POST">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <input class="input-submit" type="submit" id="delete-post-{{ $post->id }}" value="Delete" />
                        </form>
                        <p class="created-time"><?= $post->created_at; ?></p>
                        <p class="post-heading">
                            <?php foreach(explode(',', $post->heading) as $key => $value): ?>
                                <span class="heading"><?php echo array_search($value, $headings) ?></span>
                            <?php endforeach ?>
                        </p>
                    </div>
                @endforeach
            </div>
            <?php if (!isset($noPaginate)):  ?>
                <div class="paginate"><?php echo $posts->render(); ?></div>
            <?php endif ?>
        @endif
    </div>
@endsection


