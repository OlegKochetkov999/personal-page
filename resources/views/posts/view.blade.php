@extends('layouts.app')
@section('css')
    <link href="{{ URL::asset('/css/post.css') }}" rel="stylesheet" type="text/css"/>
    <link type="text/css" rel="stylesheet" href="{{ URL::asset('bower_components/SyntaxHighlighter/styles/shCore.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ URL::asset('bower_components/SyntaxHighlighter/styles/shThemeDefault.css') }}" />
    
    <script type="text/javascript" src="{{ URL::asset('bower_components/SyntaxHighlighter/scripts/XRegExp.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('bower_components/SyntaxHighlighter/scripts/shCore.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('bower_components/SyntaxHighlighter/scripts/shBrushPhp.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('bower_components/SyntaxHighlighter/scripts/shBrushCpp.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('bower_components/SyntaxHighlighter/scripts/shBrushJScript.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('bower_components/SyntaxHighlighter/scripts/shBrushXml.js') }}"></script>
    <script type="text/javascript">SyntaxHighlighter.all();</script>
@endsection
@section('menu')
    <input id="menu" type="hidden" value="post" />
@endsection
@section('content')
    <div class="container">
        <div class="post-block">
            <div class="container-title post-view">
                <div class="container-content">
                    <div class="block-description">
                        <span class="description"><a href="/" class="nav">Главная</a> <span>/ {{ $post->title }}</span>
                    </div>
                    <div class="icon-medium">
                        <svg viewbox="0 0 30 30" fill="white"><use xlink:href="#{{ $post->type }}"></use></svg>
                    </div>
                </div>
            </div>  
            <input id="post_id" type="hidden" value="{{ $post->id }}"/>
            <p class="post-heading">
                <?php foreach(explode(',', $post->heading) as $key => $value): ?>
                    <span class="heading"><?php echo array_search($value, $headings) ?></span>
                <?php endforeach ?>
            </p>
            <div class="post-content"><?= $post->content; ?></div>
        </div>
        <div class="create-comment-block">
            <p style="margin-bottom: 5px;margin-top:0px">Комментарии</p>
            <form action="{{ URL::to('comment/create') }}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="post_id" value="{{ $post->id }}" />
                <input class="input-text" name="author" placeholder="Имя" />
                <textarea class="textarea" name="content" placeholder="Комментарий"></textarea>
                <input class="input-submit"  type="submit" value="Отправить"/>
            </form>
            <div id="block_comments"></div>
        </div>
        <?php $index = 1; ?>
        @foreach($comments AS $comment)
        <div id="comment-{{ $comment->id }}" class="comments-block">
            <?php if(isset($user) and $user->name == "admin"): ?>
                <input class="remove-comment input-submit" type="button" data-id="{{ $comment->id }}" value="Remove" />
            <?php endif ?>
            #<?= $index++; ?><span class="comment-created">{{ $comment->created_at }}</span>
            <p class="comment-author">{{ $comment->author }}</p>
            <p class="comment-content">{{ $comment->content }}</p>
        </div>
        @endforeach
    </div>
@endsection
@section('js')<script src="{{ URL::asset('js/post.js') }}"></script>
@endsection