<div class="list">
    @foreach($posts as $post)
        <div id="post-<?= $post->id ?>" class="post">
            <div class="icon">
                <svg viewbox="-5 -5 40 40" fill="green"><use xlink:href="#{{ $post->type }}"></use></svg>
            </div>
            <a class="post-title" href="post/{{ $post->id }}"><?= $post->title; ?></a>
            <p class="created-time"><?= $post->created_at; ?></p>
            <p class="post-heading">
                <?php foreach(explode(',', $post->heading) as $key => $value): ?>
                    <span data-heading="{{ $value }}" class="heading"><?php echo array_search($value, $headings) ?></span>
                <?php endforeach ?>
            </p>
            <?php if($post->description): ?>
                <div class="post-description">{{ $post->description }}</div>
            <?php endif; ?>
        </div>
    @endforeach
</div>
<div class="paginate"><?php echo $posts->render(); ?></div>


