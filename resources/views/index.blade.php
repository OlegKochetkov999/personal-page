@extends('layouts.app')
@section('css')
    <link href="{{ URL::asset('bower_components/select2/select2.css') }}" rel="stylesheet" type="text/css"/>
@endsection
@section('menu')
    <input id="menu" type="hidden" value="main" />
@endsection

@section('content')
    <div class="container">
        <div class="container-title">
            <div class="container-content">
                <div class="block-description">
                    <span class="description">Блог</span>
                </div>
                <div class="container-service">
                    <input id='input-search' placeholder='Поиск по ключевым словам' />
                    <select id="input-category" class="js-example-basic-multiple" multiple="true" placeholder="Выберите категории из списка">
                        <?php foreach($headings as $key => $value): ?>
                            <option value="<?= $value; ?>"><?= $key; ?></option>
                        <?php endforeach; ?>
                    </select>    
                </div>
            </div>
        </div>    
        @if(count($posts) > 0)
            @include('posts.posts')
        @endif
    </div>
@endsection
@section('js')
<script src="{{ URL::asset('bower_components/select2/select2.js') }}"></script>
<script src="{{ URL::asset('js/post.list.js') }}"></script>
@endsection

