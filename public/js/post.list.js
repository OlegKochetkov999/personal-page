var appPostList;
    
$(function(){
    appPostList = new AppPostList();
});

function AppPostList(){
    
    this.rows = "div.list div.post";
    this.inputSearch = $("#input-search");
    this.inputCategory = $("#input-category");
    this.hideClass = "hide";
    var that = this;
    
    $.expr[":"].contains = $.expr.createPseudo(function(arg) {
        return function( elem ) {
            return $(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
        };
    });
    
    function bind(){
        $(document).on("mouseenter", that.rows, function(e){
            var description = $(this).find(".post-description")
            if(description.css("display") == "none"){
                description.show("slow");
            }
        });
        
        $(document).on("mouseleave", that.rows, function(e){
            $(this).find(".post-description").hide("slow");
        });
        
        /**
         * hide and show blocks by change input value 
         */
        that.inputSearch.on("keyup", function(){
            var val = that.inputSearch.val();
            if(val != ""){
                $.each($(that.rows), function(key, value){
                    var contains = $("#" + value.id + ":contains(" + that.inputSearch.val() + ")");
                    if(contains.length == 0){
                        value.className += " " + that.hideClass;
                    }else{
                        value.className = value.className.replace(that.hideClass, "");
                    }
                });
            }else{
                $(that.rows).removeClass(that.hideClass);
            }
        });
        
        that.inputCategory.on("change", function(){
            var headings = $(this).val();
            $.ajax({
                url: "/posts/headings",
                data: {"headings": headings},
                success: function(response){
                    $("div.paginate").remove();
                    $("div.list").remove();
                    $(".container").append(response);
                },
            });
            
        });
    };
    
    function init(){
        bind();
    };
    
    init();
};