var appCode;

$(function(){
    appCode = new Code();
});

function Code(){
    this.iframeId = '#block-iframe';
    this.blockMap = $(".main-map");
    this.blockEditor = $("#editor");
    this.iframe;
    this.script;
    this.editor;
    var that = this;
    
    function getCode(){
        that.iframe = $(that.iframeId);
        that.script = that.iframe.contents().find("div#custom").find("script");
        that.editor.getSession().setValue('');
        that.editor.getSession().setValue(that.script.html().trim());
    };
    
    function bind(){
        $(document).on('ready', that.iframeId, function(){
            setTimeout(getCode, 400);
        });
        
        $("#code-run").on("click", function(){
            var content = that.editor.getSession().getValue();
            $(that.iframeId).contents().find("#map").remove();
            $(that.iframeId).contents().find("#script").remove();
            $(that.iframeId).contents().find("#map-block").html("<div id='map'></div>");
            $(that.iframeId).contents().find("#custom").html("<script id='#script'>" + content + "</script>");
        });
        
        $("#menu-example").on("change", function(){
            $(that.iframeId).attr("src", "/map/block/" + $(this).val());
            $(that.iframeId).trigger("ready");
        });    
    };
    
    this.init = function(){
        that.blockMap.css("visibility", "visible");
        that.blockEditor.css("visibility", "visible");
        that.editor = ace.edit("editor");
        that.editor.getSession().setMode("ace/mode/javascript");
        that.editor.setBehavioursEnabled(false);

        bind();
        $(that.iframeId).trigger("ready");
    };
    
    that.init();
}