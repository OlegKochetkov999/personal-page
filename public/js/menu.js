var menu;
$.fn.extend({
        animateCss: function (animationName) {
            var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
            this.addClass('animated ' + animationName).one(animationEnd, function() {
                $(this).removeClass('animated ' + animationName);
            });
        }
    });
    
$(function(){
    menu = new Menu();
});

function Menu(){
    
    var activeClass = "menu-button-active";
    var hideClass = "hide";
    var $title;
    
    /**
     * Get set menu button by page type
     * @param {String} page
     * @returns {Array}
     */
    function menuSet(page){
        switch(page){
            case 'main':
                $(".js-example-basic-multiple").select2();
                return ['search', 'category', 'map', 'linkedin', 'email'];
                break;
            case 'post':
                return ['map', 'linkedin', 'email'];
            case 'map':
                return ['linkedin', 'email'];    
            default:
                return null;
                break;
        }
    };
    
    function activate(type){
        switch(type){
            case "search":
                $(".container-title").find(activeClass).removeClass(activeClass);
                $("div.search").addClass(activeClass);
                $('#input-search').show(100);
                $title.html("Поиск");
                break;
            case "category":
                $("div.category").addClass(activeClass);
                $('div.js-example-basic-multiple').show(100);
                $title.html("Фильтр по категориям");
                break;
            default:
                break;
        }
    };
    
    function deactivate(type){
        switch(type){
            case "search":
                $("div.search").removeClass(activeClass);
                $('#input-search').hide(100);
                $title.html("Блог");
                break;
            case "category":
                $("div.category").removeClass(activeClass);
                $('div.js-example-basic-multiple').hide(100);
                $title.html("Блог");
                break;
            default:
                break;
        }
    };
    
    function showEmailPopup(){
        $('body').css("overflow-y", "hidden");
        $('#email-modal, #background-modal').removeClass(hideClass);
        $('#background-modal').animate({opacity: 0.5});
    };
    
    function hideEmailPopup(){
        $('body').css("overflow-y", "auto");
        $('#background-modal').animate({opacity: 0});
        $('#email-modal, #background-modal').addClass(hideClass);
    };
    
    function bind(){
        $('body').on('mousewheel', function(e) {
            if (e.target.id == 'el' || $("#background-modal").hasClass(hideClass)) return;
                e.preventDefault();
                e.stopPropagation();
            }
        );

        $(document).ready(function(){
            $title = $(".block-description .description");
            
            var set = menuSet($("input#menu").val());
            if(set != null){
                $.each(set, function(key, value){
                    $("div#menu-" + value).show();
                    $("div#menu-" + value).animateCss('bounceInRight');
                });
                
            }
        });
        
        $(document).on("click", "div.search", function(){
            if($(this).hasClass(activeClass)){
                deactivate("search");
            }else{
                deactivate("category");
                activate("search");
            }
        });

        $(document).on("click", "div.category", function(){
            if($(this).hasClass(activeClass)){
                deactivate("category");
            }else{
                deactivate("search");
                activate("category");
            }
        });

        $(document).on('click', '#menu-email', showEmailPopup);
        $(document).on('click', '#background-modal', hideEmailPopup);

        $('#email-form').on('submit', function(e) {
            e.preventDefault();
            $.ajax({
                url: $(this).attr("action"),
                type: "POST",
                data: $(this).serializeArray(),
                success: function(response){
                    var data = JSON.parse(response);
                    
                    if (data.status) {
                        hideEmailPopup();
                    }
                    
                    if (data.message) {
                        $("#notice-message").html(data.message);
                        $("#notice").removeClass(hideClass);
                    }
                },
                error: function(e){
                    console.log(e);
                },
                complete: function(){
                    setTimeout(function(){
                        $("#notice").addClass(hideClass);
                    }, 3000);
                }
            });
        });
    };
    
    function init(){
        bind();
    };
    
    init();
};