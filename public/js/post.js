var appPost;
    
$(function(){
    appPost = new AppPost();
});

function AppPost(){
    var that = this;
    
    function bind(){
        $(document).on("click", ".remove-comment", function(e){
            var id = this.dataset.id;
            if (id) {
                $.ajax({
                    url: `/comment/remove/${id}`,
                    success: function (response){
                        var data = JSON.parse(response);
                        if (data.status == "ok") {
                            $(`#comment-${id}`).remove();
                        }
                    },
                    error: function(e){
                        console.log(e.responseText);
                    }
                });
            }
        });
    };
    
    function init(){
        bind();
    };
    
    init();
};