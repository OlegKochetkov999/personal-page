<?php

namespace App;

class Heading
{
    public static $headings = [
                              'SQL' => '1a', 
                              'MS SQL' => '2a', 
                              'PHP' => '3a', 
                              'Javascript' => '4a', 
                              'JQuery' => '5a', 
                              'Java' => '6a', 
                              'Linux' => '7a',
                              'PostgreSQL' => '8a', 
                              'MySQL' => '9a',
                              'Python' => '1b',
                              'Java' => '2b',
                              'Symfony' => '3b',
                              'Laravel' => '4b',
                              'Yii' => '5b',
                              'Codeigniter' => '6b',
                              'Zend Framework' => '7b',
                              'Apache' => '8b',
                              'IIS' => '9b',
                              'Software' => '1c',
                              'Windows' => '2c',
                              'Html' => '3c',
                              'Twig' => '4c'
                            ];
}
