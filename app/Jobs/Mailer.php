<?php

namespace App\Jobs;

class Mailer {
    public static function send($subject , $content)
    {
        $mail = new \PHPMailer();

        $mail->isSMTP();
        $mail->Host = 'smtp.gmail.com';
        $mail->SMTPAuth = true;
        $mail->Username = '';
        $mail->Password = '';
        $mail->SMTPSecure = 'tls';
        $mail->Subject = $subject;
        $mail->addAddress('');
        $mail->WordWrap = 50;
        $mail->Body = $content;

        return $mail->send();
    }
}
