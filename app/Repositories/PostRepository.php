<?php

namespace App\Repositories;

use App\Post;

class PostRepository
{
    /**
     * Get all of the posts for a given param.
     *
     * @param string $field
     * @param  string $value
     * @return Collection
     */
    public function getPostLike($field, $value)
    {
        return Post::like($field, $value)
                    ->orderBy('created_at', 'asc')
                    ->get();
    }
    
    /**
     * Get all of the posts for a given param.
     *
     * @param string $field
     * @param string $value
     * @return Collection
     */
    public function getPostBy($field, $value)
    {
        return Post::where($field, "=", $value)->get();
    }
    
    /**
     * Get posts by categories
     *
     * @param string $headings
     * @return Collection
     */
    public function getPostByHeadings($headings, $count)
    {
        $query = "";
        $countHeadings = count($headings);
        foreach ($headings as $key => $value) {
            $query .= " LOCATE('$value', heading) ";
            $query .= ($key != $countHeadings - 1) ? "and" : "";
        }
        
        return Post::whereRaw($query)->orderBy("created_at", "desc")->paginate($count);
    }
    
    /**
     * Get all of the posts for a given param.
     *
     * @param int|string $id
     * @return Post
     */
    public function getPostById($id)
    {
        return Post::find($id);
    }
    
    /**
     * Get all of the posts
     *
     * @return Collection
     */
    public function getAll()
    {
        return Post::orderBy('created_at', 'DESC')->get();
    }
    
    /**
     * Get paginate of the posts
     *
     * @return Collection
     */
    public function paginate($count)
    {
        return Post::paginate($count);
    }
    
    /**
     * Create new post
     *
     * @param array $data
     * @return bool
     */
    public function create($data)
    {
        return Post::create($data);
    }
}
