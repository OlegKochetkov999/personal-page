<?php

namespace App\Repositories;

use App\Comment;

class CommentRepository
{
    /**
     * Get all of the posts for a given param.
     *
     * @param string $field
     * @param string $value
     * @return Collection
     */
    public function getCommentsByPost($post_id)
    {
        return Comment::where("post_id", "=", $post_id)->orderBy("created_at", "ASC")->get();
    }
    
    /**
     * Create new comment
     *
     * @param array $data
     * @return bool
     */
    public function create($data)
    {
        return Comment::create($data);
    }
    
    /**
     * Get Comment by id
     *
     * @param int $id
     * @return Comment
     */
    public function findById($id)
    {
        return Comment::query()->find($id);
    }
}
