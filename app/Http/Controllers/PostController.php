<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth as Auth;
use App\Http\Controllers\Controller;

use App\Post;
use App\Repositories\CommentRepository;
use App\Heading;
use App\Type;
use App\Repositories\PostRepository;

class PostController extends Controller
{
    /**
     * The post repository instance.
     *
     * @var PostRepository
     */
    protected $posts;

    protected $user;
    
    /**
     * Count pages for pagination
     * @var int
     */
    protected $pages = 10;
    
    /**
     * Create a new controller instance.
     *
     * @param  PostRepository  $posts
     * @return void
     */
    public function __construct(PostRepository $posts)
    {
        $this->user = Auth::user();
        $this->posts = $posts;
    }

    /**
     * Display a list of all of the user's task.
     *
     * @param  Request  $request
     * @return Response
     */
    public function index(Request $request)
    {
        $data = ['posts' => $this->posts->paginate($this->pages), 'headings' => Heading::$headings];
        $template = ($this->user AND $this->user->name == "admin") ? 'posts.list' : 'index';
        return view($template, $data);
    }

    /**
     * Create a new post.
     *
     * @param  Request  $request
     * @return Response
     */
    public function create(Request $request)
    {
        if ($this->user AND $this->user->name == "admin") {
            $data = ['title' => 'required|max:255', 'description' => 'max:255', 'heading' => 'required'];
            $this->validate($request, $data);
            $this->posts->create([
                                'title' => $request->title, 
                                'heading' => implode(',' , $request->heading), 
                                'content' => $request->content,
                                'description' => $request->description,
                                'type' => $request->type
                            ]);

            return redirect('/posts');
        } else {
            return redirect('/');
        }
    }

    /**
     * Edit post.
     *
     * @param  Request  $request
     * @param  string $id
     * @return Response
     */
    public function edit(Request $request, $id)
    {
        if ($this->user AND $this->user->name == "admin") {
            if ($request->method() == "POST") {
                if ($id != 0) {
                    $post = $this->posts->getPostById($id);
                    $post->title = $request->title; 
                    $post->content = $request->content;
                    $post->description = $request->description;
                    $post->type = $request->type; 
                    $post->heading = implode(',' , $request->heading); 
                    $post->save();
                }
                
                return view('posts.list', ['posts' => $this->posts->getAll(), 'headings' => Heading::$headings, "noPaginate" => true]);
            } else if($request->method() == "GET") {
                return view('posts.edit', ['post' => $this->posts->getPostById($id), 'headings' => Heading::$headings, 'types' => Type::$types]);
            }
        } else {
            return redirect('/');
        }
    }
    
    /**
     * Get posts by categories
     * @param Request $request
     */
    public function posts(Request $request)
    {
        if ($request->headings) {
            $posts = $this->posts->getPostByHeadings($request->headings, $this->pages);
            return view('posts.posts', ['posts' => $posts, 'headings' => Heading::$headings]);
        }
        
        return view('posts.posts', ['posts' => $this->posts->paginate($this->pages), 'headings' => Heading::$headings]);
    }
    
    /**
     * Remove the given post.
     *
     * @param  Request  $request
     * @param  Post  $post
     * @return Response
     */
    public function remove(Request $request, Post $post)
    {
        if ($this->user AND $this->user->name == "admin") {
            $post->delete();
            return redirect('/posts');
        } else {
            return redirect('/');
        }
    }
    
    /**
     * Get post.
     *
     * @param  Request $request
     * @param  Int  $id
     * @return Response
     */
    public function post(Request $request, $id)
    {
        $comments = new CommentRepository(); 
        return view('posts.view', [
                                    'post' => $this->posts->getPostById($id), 
                                    'headings' => Heading::$headings,
                                    'comments' => $comments->getCommentsByPost($id),
                                    'user' => $this->user
        ]);
    }        
}
