<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Post;
use App\Heading;
use App\Repositories\PostRepository;
use App\Jobs\Mailer;

class MainController extends Controller
{
    /**
     * The post repository instance.
     *
     * @var PostRepository
     */
    protected $posts;

    /**
     * Create a new controller instance.
     *
     * @param  PostRepository  $posts
     * @return void
     */
    public function __construct(PostRepository $posts)
    {
        $this->posts = $posts;
    }

    /**
     * Display a list of all of the user's task.
     *
     * @param  Request  $request
     * @return Response
     */
    public function emailSend(Request $request)
    {
        if(!Mailer::send("[BLOG] from $request->email", $request->content)) {
            echo json_encode(["status" => false, "message" => 'Произошел сбой, письмо не было отправлено. Попробуйте позже']);
        } else {
            echo json_encode(["status" => true, "message" => 'Письмо успешно отправлено']);
        }
    }
}
