<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Repositories\PostRepository;

class MapController extends Controller
{
    private static $types = [1 => "init-map", 
                             2 => "init-controls",
                             3 => "bind-popup",
                             4 => "custom-marker",
                             10 => "custom-control",
                             5 => "centering",
                             6 => "scaling",
                             7 => "route",
                             8 => "geocoding",
                             9 => "track"
                            ];

    private static $typesName = [1 => "Инициализация карты", 
                                 2 => "Элементы управления на карте",
                                 3 => "Всплывающее окно",
                                 4 => "Пользовательские маркеры",
                                 10 => "Пользовательский контрол",
                                 5 => "Центрирование",
                                 6 => "Масштабирование",
                                 7 => "Маршруты",
                                 8 => "Геокодирование",
                                 9 => "Трэки движения"
                                ];

    /**
     * Display a list of all of the user's task.
     *
     * @param  Request  $request
     * @return Response
     */
    public function index(Request $request)
    {
        return view('map.index', ["types" => self::$typesName]);
    }
    
    /**
     * Display a list of all of the user's task.
     *
     * @param  Request  $request
     * @return Response
     */
    public function block(Request $request, $id)
    {
        $type = isset(self::$types[$id]) ? self::$types[$id] : self::$types[1];
        return view("map.examples.$type");
    }
}
