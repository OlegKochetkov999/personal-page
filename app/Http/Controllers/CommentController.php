<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Jobs\Mailer;

use App\Comment;
use App\Repositories\CommentRepository;

class CommentController extends Controller
{
    /**
     * The comment repository instance.
     *
     * @var CommentRepository
     */
    protected $comments;

    /**
     * Create a new controller instance.
     *
     * @param  CommentRepository  $comments
     * @return void
     */
    public function __construct(CommentRepository $comments)
    {
        $this->comments = $comments;
    }

    /**
     * Create a new post.
     *
     * @param  Request  $request
     * @return Response
     */
    public function create(Request $request)
    {
        $this->validate($request, ['content' => 'required']);
        $this->comments->create([
                            'post_id' => $request->post_id, 
                            'author' => $request->author ? $request->author : "Аноним", 
                            'content' => $request->content
                        ]);
        
        $post = $request->post_id;
        if ($server = $request->server() and array_key_exists("HTTP_REFERER", $server)) {
            $post = $server["HTTP_REFERER"];
        }
        
        Mailer::send("[BLOG] New comment", "[$post] $request->author: $request->content");
        return redirect("/post/$request->post_id");
    }

    /**
     * Get Json comments by id post
     * @param Request $request
     * @param string $post
     */
    public function getCommentsByPost(Request $request, $post)
    {
        echo json_encode($this->comments->getCommentsByPost($post));
    }
    
    /**
     * Remove the given comment by id
     *
     * @param  Request  $request
     * @param  int $id
     * @return Response
     */
    public function remove(Request $request, $id)
    {
        $result = ["status" => "error"];
        $comment = $this->comments->findById($id);
        if ($comment and $comment instanceof Comment) {
            if ($comment->delete()) {
                $result["status"] = "ok";
            }
        }
        
        return new Response(json_encode($result));
    } 
}
