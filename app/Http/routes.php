<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {

    Route::get('/', 'PostController@index');
    Route::post('/email/send', 'MainController@emailSend');

    Route::get('/map', 'MapController@index');
    Route::get('/map/block/{id}', 'MapController@block');
    
    Route::get('/posts', 'PostController@index');
    Route::get('/posts/headings', 'PostController@posts');
    Route::get('/post/{id}', 'PostController@post');
    Route::post('/post/create', 'PostController@create');
    Route::any('/post/edit/{id}', 'PostController@edit');
    Route::delete('/post/remove/{post}', 'PostController@remove');

    Route::get('/comment/by-post/{post}', 'CommentController@getCommentsByPost');
    Route::post('/comment/create', 'CommentController@create');
    Route::get('/comment/remove/{id}', 'CommentController@remove');
    Route::auth();

});
